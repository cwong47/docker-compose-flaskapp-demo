FROM python:3-alpine
ADD . /code
WORKDIR /code
RUN pip install flask redis
CMD ["python", "app.py"]

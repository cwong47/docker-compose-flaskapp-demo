# Description
This directory contains docker related files to build a python flask app using docker-compose.

# Docker
This app will be using python and talking to redis.

## CI using docker-compose
This utilize the docker-compose.yml file.
```
$ docker-compose up
```

# Source
More info [here](https://docs.docker.com/compose/gettingstarted/).

# License and Authors
Authors: Calvin Wong
